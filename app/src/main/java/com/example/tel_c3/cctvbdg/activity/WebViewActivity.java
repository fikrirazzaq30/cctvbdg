package com.example.tel_c3.cctvbdg.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.tel_c3.cctvbdg.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Tel-C 3 on 12/8/2016.
 */
public class WebViewActivity extends AppCompatActivity {

    WebView wb;
    ProgressBar pg;
    private ShareActionProvider mShareActionProvider;

    FloatingActionButton fab, fab2;
    ImageView imageView;

    private Bitmap mBitmap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        wb = (WebView) findViewById(R.id.webViewEuy);
//        pg = (ProgressBar) findViewById(R.id.progressBarEuy);

        wb.getSettings().setJavaScriptEnabled(true);
        wb.setWebViewClient(new WebViewClient());

        wb.getSettings().setDomStorageEnabled(true);
        wb.getSettings().setLoadWithOverviewMode(true);
        wb.getSettings().setUseWideViewPort(true);
        wb.getSettings().setSupportMultipleWindows(true);
        wb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wb.setHorizontalScrollBarEnabled(false);
        wb.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        wb.getSettings().setAllowFileAccessFromFileURLs(true);
        wb.getSettings().setAllowUniversalAccessFromFileURLs(true);

//        wb.setWebChromeClient(new WebChromeClient() {
//            @Override
//            public void onProgressChanged(WebView view, int newProgress) {
//                pg.setVisibility(View.VISIBLE);
//                pg.setProgress(newProgress);
//                if (newProgress == 100) {
//                    pg.setVisibility(View.GONE);
//                }
//            }
//        });

        wb.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                try {
                    view.stopLoading();
                }
                catch(Exception e){}
                view.clearView();

                //Toast.makeText(getApplicationContext(), "Error code is "+errorCode, Toast.LENGTH_SHORT).show();
                if(errorCode == -2 || errorCode == -8) {
                    view.loadData("<!DOCTYPE html>\n" +
                            "<html>\n" +
                            "<head>\n" +
                            "<style>\n" +
                            ".center {\n" +
                            "    padding: 70px 0;\n" +
                            "    border: 0px solid green;\n" +
                            "    text-align: center;\n" +
                            "}\n" +
                            "</style>\n" +
                            "</head>\n" +
                            "<body>\n" +
                            "\n" +
                            "<div class=\"center\">\n" +
                            "  <img src=\"http://www.iconsplace.com/download/orange-error-256.png\" alt=\"Terjadi Kesalahan!\" style=\"width:64px;height:64px;\" align=\"center\">\n" +
                            "<h1>Terdapat masalah dengan koneksi internet Anda. Silahkan coba lagi nanti.</h1>\n" +
                            "</div>\n" +
                            "\n" +
                            "</body>\n" +
                            "</html>\n", "text/html", "UTF-8");
                }

                if(errorCode == -14 || errorCode == -6) {
                    view.loadData("<!DOCTYPE html>\n" +
                            "<html>\n" +
                            "<head>\n" +
                            "<style>\n" +
                            ".center {\n" +
                            "    padding: 70px 0;\n" +
                            "    border: 0px solid green;\n" +
                            "    text-align: center;\n" +
                            "}\n" +
                            "</style>\n" +
                            "</head>\n" +
                            "<body>\n" +
                            "\n" +
                            "<div class=\"center\">\n" +
                            "  <img src=\"http://www.iconsplace.com/download/orange-error-256.png\" alt=\"Terjadi Kesalahan!\" style=\"width:64px;height:64px;\" align=\"center\">\n" +
                            "<h1>Maaf, server sedang mengalami gangguan.</h1>\n" +
                            "</div>\n" +
                            "\n" +
                            "</body>\n" +
                            "</html>\n", "text/html", "UTF-8");
                }
            }
        });

        Intent intent = getIntent();

        wb.loadUrl("http://" + intent.getStringExtra(ListActivity.IP_PUB) + ":"
                + intent.getStringExtra(ListActivity.PORT) + "/cgi-bin/faststream.jpg");

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setBackgroundTintList(getResources().getColorStateList(android.R.color.white));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screenShot(view);
                shareViaFab(view);
            }
        });

        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setBackgroundTintList(getResources().getColorStateList(android.R.color.white));
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imageView = (ImageView) findViewById(R.id.imageView2);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void screenShot(View view) {
        mBitmap = getBitmapOFRootView(fab);
        imageView.setImageBitmap(mBitmap);
        createImage(mBitmap);
    }

    public Bitmap getBitmapOFRootView(View v) {
        View rootview = v.getRootView();
        rootview.setDrawingCacheEnabled(true);
        Bitmap bitmap1 = rootview.getDrawingCache();
        return bitmap1;
    }

    public void createImage(Bitmap bmp) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        File file = new File(Environment.getExternalStorageDirectory() +
                "/cctvbdg.jpg");
        try {
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(bytes.toByteArray());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void shareViaFab(View view) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpg");
        final File photoFile = new File(Environment.getExternalStorageDirectory(), "cctvbdg.jpg");
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(photoFile));
        shareIntent.putExtra(Intent.EXTRA_TEXT, "#cctvbdg");
        startActivity(Intent.createChooser(shareIntent, "Share..."));
    }
}