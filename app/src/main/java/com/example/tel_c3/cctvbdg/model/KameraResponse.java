package com.example.tel_c3.cctvbdg.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by TEL-C 1 on 12/28/2016.
 */
public class KameraResponse {
    @SerializedName("kamera")
    private List<Kamera> kamera;

    public List<Kamera> getKamera() {
        return kamera;
    }

    public void setKamera(List<Kamera> kamera) {
        this.kamera = kamera;
    }
}