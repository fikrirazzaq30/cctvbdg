package com.example.tel_c3.cctvbdg.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tel_c3.cctvbdg.R;
import com.example.tel_c3.cctvbdg.adapter.KameraAdapter;
import com.example.tel_c3.cctvbdg.adapter.RecyclerItemClickListener;
import com.example.tel_c3.cctvbdg.model.Kamera;
import com.example.tel_c3.cctvbdg.model.KameraResponse;
import com.example.tel_c3.cctvbdg.rest.ApiClientKamera;
import com.example.tel_c3.cctvbdg.rest.ApiInterfaceKamera;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Tel-C 3 on 1/23/2017.
 */
public class ListActivity extends AppCompatActivity {
    Intent intent;
    public static final String IP_PUB = "ip_pub";
    public static final String PORT = "port";

    private static final String TAG = MainActivity.class.getSimpleName();

    List<Kamera> kameras;

    FloatingActionButton fab2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.kamera_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        final ProgressDialog loading = ProgressDialog.show(ListActivity.this,"Memuat Data","Harap Tunggu..",false,false);
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        ApiInterfaceKamera apiService =
                ApiClientKamera.getClient().create(ApiInterfaceKamera.class);

        Call<KameraResponse> call = apiService.getDataKamera();
        call.enqueue(new Callback<KameraResponse>() {
            @Override
            public void onResponse(Call<KameraResponse> call, Response<KameraResponse> response) {
                loading.dismiss();
                int statusCode = response.code();
                kameras = response.body().getKamera();
                recyclerView.setAdapter(new KameraAdapter(kameras, R.layout.list_item, getApplicationContext()));
            }

            @Override
            public void onFailure(Call<KameraResponse> call, Throwable t) {
                Log.e(TAG, t.toString());
                Toast.makeText(ListActivity.this, "Terjadi kesalahan, data gagal dimuat!", Toast.LENGTH_SHORT).show();
            }
        });

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        intent = new Intent(ListActivity.this, WebViewActivity.class);

                        Kamera kamera = kameras.get(position);
                        intent.putExtra(IP_PUB, kamera.getIpPub());
                        intent.putExtra(PORT, kamera.getPort());
                        Toast.makeText(ListActivity.this, kamera.getNamaLokasi(), Toast.LENGTH_SHORT).show();

                        startActivity(intent);

                    }
                })
        );

        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
