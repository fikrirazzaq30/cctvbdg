package com.example.tel_c3.cctvbdg.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tel_c3.cctvbdg.R;
import com.example.tel_c3.cctvbdg.model.Kamera;

import java.util.List;


/**
 * Created by Tel-C 3 on 2/5/2017.
 */
public class KameraAdapter extends RecyclerView.Adapter<KameraAdapter.KameraViewHolder>{

    private List<Kamera> kameras;
    private int rowLayout;
    private Context context;

    public static class KameraViewHolder extends RecyclerView.ViewHolder {

        LinearLayout kameraLayout;
        TextView namaLokasi;

        public KameraViewHolder(View itemView) {
            super(itemView);

            kameraLayout = (LinearLayout) itemView.findViewById(R.id.kamera_layout);
            namaLokasi = (TextView) itemView.findViewById(R.id.nama_lokasi);
        }
    }

    public KameraAdapter(List<Kamera> kameras, int rowLayout, Context context) {
        this.kameras = kameras;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public KameraAdapter.KameraViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new KameraViewHolder(view);
    }

    @Override
    public void onBindViewHolder(KameraViewHolder holder, int position) {
            holder.namaLokasi.setText(kameras.get(position).getNamaLokasi());
    }

    @Override
    public int getItemCount() {
        return kameras.size();
    }
}
