package com.example.tel_c3.cctvbdg.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TEL-C 1 on 12/28/2016.
 */
public class Kamera {
    @SerializedName("nama_lokasi")
    private String namaLokasi;
    @SerializedName("ip_pub")
    private String ipPub;
    @SerializedName("port")
    private String port;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;
    @SerializedName("merk")
    private String merk;

    public Kamera(String namaLokasi, String ipPub,
                  String port, String lat, String lng, String merk) {
        this.namaLokasi = namaLokasi;
        this.ipPub = ipPub;
        this.port = port;
        this.lat = lat;
        this.lng = lng;
        this.merk = merk;
    }

    public String getNamaLokasi() {
        return namaLokasi;
    }

    public void setNamaLokasi(String namaLokasi) {
        this.namaLokasi = namaLokasi;
    }

    public String getIpPub() {
        return ipPub;
    }

    public void setIpPub(String ipPub) {
        this.ipPub = ipPub;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }
}
